/**
 * login
 * register 
 * 
 * Security related: protect yourself from brute force attacks:
 * NGINX:
 * https://acte.ltd/blog/protect-application-login-brute-force-attacks#:~:text=Nginx%20is%20able%20to%20handle,limiting%20the%20attempt%20per%20second.
 */

import MailSendgrid from '../../controllers/mailsendgrid.js'
import MailLocal from '../../controllers/mailsmtp.js'
import Authentication from '../../controllers/authentication.js'

const axios = require('axios')

const { Router } = require('express')

const passport = require('passport')
const router = Router()
    //set it to true if you want sendgrid to handle your email
    //otherwise the local mail setup will be used
const useSendgrid = false

/**
 * Register a new user
 * only send back the result IE:
 * email already in use
 * account created, check email
 * The registration opbject contains the whole from
 * this should make it easier to build your own registration form
 * and handle it when creating the user without having to change code all over
 * the place
 */

router.post('/register', async(req, res) => {
    const resultMessage = await Authentication.registerUser(req.body.registration)
    res.send({ message: resultMessage })
})

/**
 * confirmation of the user registration
 */
router.post('/confirmation', async(req, res) => {
    const token = req.body.token
    const { email, verificationToken } = Authentication.verifySignedVerificationToken(token)
    const user = await Authentication.GetUser(email)

    if (user && user.verificationToken === verificationToken && user.verificationTokenExpire >= new Date()) {
        user.isVerified = true
        user.save()
        return res.send({
            confirmationStatus: 'verified',
            message: 'Your email has been verified.'
        })
    } else {
        return res.send({
            confirmationStatus: 'unverified',
            message: 'Email can\'t be verified!\n. The possible reason is expired token.'
        })
    }
})

/**
 * resend confirmation
 */

router.post('/confirmation/resend', async(req, res) => {
    const email = req.body.email
    const thisUser = await Authentication.GetUser(email)

    if (thisUser && thisUser.isVerified === true) {
        return res.send({ message: 'Allready verified.' })
    } else if (thisUser) {
        const verificationToken = Authentication.generateVerificationToken()
        const verificationTokenExpire = Authentication.generateVerificationTokenExpire()

        thisUser.verificationToken = verificationToken
        thisUser.verificationTokenExpire = verificationTokenExpire
        thisUser.save()

        const signedVerificationToken = Authentication.signVerificationToken(user.email, user.verificationToken)
        if (useSendgrid) {
            await MailSendgrid.SendRegistrationToken(thisUser.email, 'Registration confirmation - resend', signedVerificationToken)
        } else {
            await MailLocal.SendRegistrationToken(thisUser.email, 'Registration confirmation - resend', signedVerificationToken)
        }
        return res.send({ message: 'Token has been resent.' })
    } else {
        return res.send({ message: 'Token can\'t be resent.' })
    }
})

/**
 * login 
 * could do with some protection against password spraying
 * in real life this should be done at the webserver level
 */
router.post('/login', (req, res) => {
    passport.authenticate('local', { session: false }, (err, user, message) => {
        if (err) {
            console.log(err)
            return res.status(500).send(message)
        } else if (!user) {
            // you should log it
            // apply your favourite loggin method
            //var message = "No Such User or wrong password "
            return res.status(403).send(message)
        } else {
            const token = Authentication.signUserToken(user)
            return res.send({ token })
        }
    })(req, res)
})

/**
 * logout
 * await this.$auth.logout()
 * Nuxt/vue implementation:
 * step 9 10 and 11
 * https://www.digitalocean.com/community/tutorials/implementing-authentication-in-nuxtjs-app
 * 
 * in nuxt: https://stackoverflow.com/questions/68282059/how-to-implement-logout-in-nuxt-js
 * 
 * 
 */
router.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/");
});


/**
 * how used
 * This call returns the current user
 * the page should contain a password change option with old/new/confirm password
 */
router.get('/user', async(req, res) => {
    console.log("Getting the user with the jwt token")
    passport.authenticate('jwt', { session: false }, (err, user, message) => {
        if (err) {
            // you should log it
            return res.status(400).send(err)
        } else if (!user) {
            // you should log it
            //"no such user found"
            return res.status(403).send({ message })
        } else if (user.isLocked) {
            console.log("User Locked Out")
                //this is where the magic should happen
                //req.logout();
                //does not work, goes into a loop
                //return res.redirect('/login/isLocked');
            return res.send({ user })
        } else {
            //console.log(user)
            return res.send({ user })
        }
    })(res, req)
})

/**
 * password reset and confirmation
 */
router.post('/password/reset', async(req, res) => {
    const email = req.body.email
    const thisUser = await Authentication.GetUser(email)
    console.log(thisUser)
    if (thisUser) {
        const verificationToken = Authentication.generateVerificationToken()
        const verificationTokenExpire = Authentication.generateVerificationTokenExpire()

        thisUser.verificationToken = verificationToken
        thisUser.verificationTokenExpire = verificationTokenExpire
        thisUser.resetPassword = true
        thisUser.save()

        const signedVerificationToken = Authentication.signVerificationToken(thisUser.email, thisUser.verificationToken)
        console.log("Signed Token Send" + signedVerificationToken)

        if (useSendgrid) {
            await MailSendgrid.SendPasswordChangeToken(thisUser.email, 'Password reset', signedVerificationToken)
        } else {
            await MailLocal.SendPasswordChangeToken(thisUser.email, 'Password reset', signedVerificationToken)
        }
        return res.send({ message: 'Link has been sent. Check you email.' })
    } else {
        return res.send({ message: 'Password can\'t be renew' })
    }
})

/**
 * change the password after reset
 * Note: logged in users can change their password too
 * see /user/password/change
 */
router.post('/password/change', async(req, res) => {
    const token = req.body.token
    const password = req.body.password
    const { email, verificationToken } = Authentication.verifySignedVerificationToken(token)
    const thisUser = await Authentication.GetUser(email)
    if (thisUser && thisUser.verificationToken === verificationToken && thisUser.verificationTokenExpire >= new Date() && thisUser.resetPassword === true) {
        try {
            thisUser.password = await Authentication.generatePasswordHash(password)
            thisUser.resetPassword = false
            thisUser.save()
            return res.send({ message: 'Password has been changed' })
        } catch (err) {
            return res.send({ message: 'Some error happened. Contact administrator' })
        }
    } else {
        return res.send({ message: 'Token is invalid!. Re-send your request' })
    }
})

/**
 * create users
 * TODO Ensure this function  is removed on production env
 * uses https://randomuser.me/
 * see there for the user data and map this to your user table
 * make a call to /api/auth/createUsers
 */

router.get('/createUsers', async function(req, res) {
    const usersToCreate = 2
    const testPassword = "test"
    var userScope
    for (var i = 0; i < usersToCreate; i++) {
        if (i == 1) {
            //first user will be admin
            userScope = 'admin'
        } else {
            userScope = 'user'
        }

        const result = await axios({ url: 'https://randomuser.me/api', method: 'get' })
        const thisUser = result.data.results[0]
        console.log(thisUser)
            //mapping to your user table
        const newUser = {
            email: thisUser.email,
            username: thisUser.login.username,
            password: testPassword,
            scope: userScope

        }
        const resultMessage = await Authentication.registerUser(newUser)

        /**
         * set the isVerified to true
         * the emails will be send but we can't be bothered with verifying
         * It will ensure we can test the email
         */
        const user = await Authentication.GetUser(newUser.email)
        if (user) {
            user.isVerified = true
            user.save()
            console.log(`${newUser.email} is created and verified`)
        }
    }
    res.send({ message: 'Users Created' })
});

export default router