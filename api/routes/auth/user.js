/**
 * user route for getting profile etc
 * see index.js for getting the logged in user 
 * (and getting to the profile page )
 */
var express = require('express');
var router = express.Router();
import Authentication from '../../controllers/authentication.js'

const { user } = require('../../models/user.js')

/**
 * function called every time the route is requested
 */
router.use(function(req, res, next) {
    console.log(req.user)
    console.log("Called for each user route")
    if (req.user && req.user.isLocked) {
        console.log(`user ${req.user.email} is locked out`)
    } // else {
    next()
        //  }
})

/* GET /user */
router.get('/', function(req, res, next) {
    //res.send('respond with a resource');
});

/**
 * get the user data, logged in user only
 * as this is an api call this will result in a 401 error
 * before it gets to this function
 * profile page is for logged in users only so that page is restricted 
 * check the middleware in pages/profile/vue
 */
router.get('/profile', async function(req, res) {
    console.log(req.user)
    console.log("calling profile")
    const thisUser = await user.findOne({ email: req.user.email }).select('-_id -password -verificationToken -verificationTokenExpire')
    res.send(thisUser);
});
/**
 * change the user pasword
 * only if the user is logged in
 * body should have original password
 * newpassword
 * newpasswordconfirm
 */
router.post('/changepassword', async function(req, res) {
    //console.log("change password")
    //console.log(req.user)
    //console.log(req.body.data)
    const thisUser = await user.findOne({ email: req.user.email })
    if (thisUser) {
        //this is where the magic happens
        const valid = await Authentication.comparePasswords(req.body.data.password, thisUser.password)
        if (valid) {
            thisUser.password = await Authentication.generatePasswordHash(req.body.data.newpassword)
            thisUser.save()
            return res.send({
                message: "Password has changed"
            })
        } else {
            res.send({ message: "Original Password does not match" })
        }
    } else {
        //should not happen so log
        return res.send({ message: "User Not found" })
    }
});

module.exports = router;