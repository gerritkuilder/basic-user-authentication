/**
 * admin route, for users with admin scope only
 * see index.js the route setup for admin and 
 * authenticator controller adminscope
 */
const { Router } = require('express')
const router = Router()

const { user } = require('../../models/user.js')

/**
 * adminpath is protected by admin scope 
 * so this should not result in data returned
 * if not logged in or the user does not have the admin scope
 */
router.get('/', async(req, res) => {
    console.log("admin only")
    const users = await user.find()
    res.send(users)
})

/**
 * userupdates etc
 */

router.post('/user/update', async(req, res) => {
    console.log(req.body.data) //submitted data
    console.log(req.user) //user making the request
        /*
        email: 'test5@asuspn51.home',
          password: '$2b$12$iGRetdDNWS5F.rLd/e4EZO.m966KLbpc5.s7iyfHIpmKiWnr3Ea02',
          verificationToken: '1bbc547c5a41f98b64c138e7d0c67d6f0f5d59738a077187d3f3313a3f71',
          verificationTokenExpire: '2021-11-25T16:55:04.181Z',
          isVerified: true,
          resetPassword: false,
          scope: 'user',
          */
        // get the fields you want to update     
    const updateUser = {
            email: req.body.data.email,
            scope: req.body.data.scope,
            isLocked: req.body.data.isLocked,
            failedLogins: req.body.data.failedLogins,
        }
        // we should not lock out if the user updating their own account
    if (req.user.email === updateUser.email) {
        updateUser.isLocked = false

    }
    // TODO: [UA-25] if isLocked changes to false trigger password reset
    user.findByIdAndUpdate(req.body.data._id, updateUser, { new: true },
        function(err, result) {
            if (err) {
                res.send({ message: `Update failed ${err}` });
            } else {
                console.log(result)
                res.send({ message: `Updated User ${req.body.data.email}` });
            }
        }
    );
})

export default router