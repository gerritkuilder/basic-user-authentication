//https: //blowstack.com/blog/authentication-in-nuxt-app-auth-module-and-express_js-implementation
require('dotenv').config()

import { connect, connection } from 'mongoose';

import express from 'express';
const passport = require('passport');
const app = express()
const server = require("http").createServer(app);
const http = require('http').Server(app);
const bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
app.use(bodyParser.json({ limit: '50mb' }));

app.use(passport.initialize())
app.use(cookieParser())

/* routes */
// TODO: [UA-14] move all the authentication requried routes into /api/auth
import Authentication from './controllers/authentication.js'
import auth from './routes/auth/authentication.js';
app.use('/auth', auth)
import admin from './routes/auth/admin.js'
//protect the route, only user with scope admin can access the route
//also when called as a direct api call
app.use('/admin', [passport.authenticate('jwt', { session: false }), Authentication.adminScope], [admin], user)

//user route
import user from './routes/auth/user.js'
//this way we get the logged in user easily in the route
app.use('/user', passport.authenticate('jwt', { session: false }), user);

/**
 * the rest can be custom
 */



/**
 * startup, set the port
 */
if (require.main === module) {
    const port = process.env.PORT || 7070
    app.listen(port, () => {
        // eslint-disable-next-line no-console
        console.log(`API server listening on port ${port}`)
    })
}

/**
 * functions for the database start/running errors
 */
connect(
    process.env.DATABASE_URL, {
        useNewUrlParser: true,
    },
    (err) => {
        console.log('mongo db connection', err)
    }
)

connection.on('connected', function() {
    console.log(`Mongoose default connection is open `)
})

connection.on('error', function(err) {
    console.log(`Mongoose default connection has occured ${err} error`)
        //Logger.error(`Mongoose default connection has occured ${err} error`, 'mongodb')
});

connection.on('disconnected', function() {
    console.log('Mongoose default connection is disconnected')
        //  Logger.fatal('Mongoose default connection is disconnected', 'mongodb')
})

export default { path: '/api', handler: app, app, server }