/**
 * use your smtp server of choice
 * in this case uing a local server.
 * No TLS, no SSL
 * this sends the basic text email
 * 
 */

// TODO: [UA-23] replace nodemairler with https://github.com/eleith/emailjs
const nodemailer = require('nodemailer')
const emailFrom = process.env.SMTPEMAILFROM
const smtpUser = process.env.SMTPUSER
const smtpPassword = process.env.SMTPPASSWORD
const smtpHost = process.env.SMTPHOST
const smtpPort = process.env.SMTPPORT // 25 insecureport, secure is 587

const emailUrl = process.env.EMAILURL
const registerConfirmation = process.env.REGISTERCONFRIMATION
const resetConfirmation = process.env.RESETCONFRIMATION
const resetPassword = process.env.RESETPASSWORD

/**
 * config 
 */
var transporter = nodemailer.createTransport({
    pool: true,
    host: smtpHost,
    port: smtpPort,
    secure: false, // do not use TLS
    auth: {
        user: smtpUser,
        pass: smtpPassword,
    },
    tls: {
        rejectUnauthorized: false //using the local smtp server
    }
});


async function SendRegistrationToken(emailTo, emailSubject, verificationToken) {
    let mailOptions = {
        from: emailFrom,
        to: emailTo,
        subject: emailSubject,
        text: `Please  follow the link to finish your registration ${emailUrl}${registerConfirmation}?token=${verificationToken}`
    };
    transporter.sendMail(mailOptions, function(err, data) {
        if (err) {
            console.log("Error " + err);
        } else {
            console.log("Email sent successfully");
        }
    });
}

async function SendPasswordChangeToken(emailTo, emailSubject, verificationToken) {
    let mailOptions = {
        from: emailFrom,
        to: emailTo,
        subject: emailSubject,
        text: `Please follow the link below to change your password ${emailUrl}${resetConfirmation}??token=${verificationToken}`
    };
    transporter.sendMail(mailOptions, function(err, data) {
        if (err) {
            console.log("Error " + err);
        } else {
            console.log("Email sent successfully");
        }
    });
}

async function SendAccountTempLock(emailTo, emailSubject, verificationToken) {
    let mailOptions = {
        from: emailFrom,
        to: emailTo,
        subject: emailSubject,
        text: `Your account has been temporary disabled due to too many failed logins. If you did not login also inform your admin. Please follow the link below to
         change your password ${emailUrl}${resetPassword}`
    };
    transporter.sendMail(mailOptions, function(err, data) {
        if (err) {
            console.log("Error " + err);
        } else {
            console.log("Email sent successfully");
        }
    });
}

export default {
    SendRegistrationToken,
    SendPasswordChangeToken,
    SendAccountTempLock
}