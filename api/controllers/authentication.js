/**
 * user creation/validation
 * authentication/registration
 * 
 * For the most part you would only need to modify the createUser function
 * depending on the data you need in the user table based on the registration form
 */


//import { userT } from '../models/user.js';
const { user } = require('../models/user.js')


import MailSendgrid from '../controllers/mailsendgrid.js'
import MailLocal from '../controllers/mailsmtp.js'



const bcrypt = require('bcrypt')
const crypto = require('crypto');

const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const JwtStrategy = require('passport-jwt').Strategy
const jwt = require("jsonwebtoken");

const authUserSecret = process.env.AUTH_USER_SECRET
const authEmailVerificationSecret = process.env.AUTH_EMAIL_VERIFICATION_SECRET
const expireSpan = 3600 * 1000 * 24

//set it to true if you want sendgrid to handle your email
//otherwise the local mail setup will be used
const useSendgrid = false


/**
 * handle the user registration here
 * return a message:
 * - Account created, check your inbox for account verifcation
 * - email already in use 
 * @param {*} inRegistration 
 * registration is the complete form data in your registration form
 */
async function registerUser(inRegistration) {
    const email = inRegistration.email
        //set the default message  to account created, 
    var thisMessage = "Account created, check your inbox to confirm"
        //validate unique user email
    const Thisuser = await user.findOne({ email: inRegistration.email })
        //if username is used too, it should also be unique 
    if (Thisuser) {
        thisMessage = "Email already in use, please use another email address or rest your account"
    } else {
        //lets continue
        const hashedPassword = await generatePasswordHash(inRegistration.password)
        await CreateUser(inRegistration, hashedPassword)
            .then(async(user) => {
                const signedVerificationToken = signVerificationToken(user.email, user.verificationToken)
                if (useSendgrid) {
                    //using sendgrid, untested
                    await MailSendgrid.SendRegistrationToken(
                        email,
                        'Registration confirmation',
                        signedVerificationToken
                    )
                } else {
                    //local email server
                    await MailLocal.SendRegistrationToken(
                        email,
                        'Registration confirmation',
                        signedVerificationToken
                    )
                }
            }).catch((err) => {
                throw err
            })
    }
    return thisMessage
}

/**
 * create the user in the database
 * your values may value
 * passwor will be encrypted
 * @param {The Form data} inRegistration
 * @param {The encrypted password} password 
 * @returns 
 */
async function CreateUser(inRegistration, password) {
    console.log(inRegistration)
        // new, not implemented yet
    inRegistration.verificationToken = generateVerificationToken()
    inRegistration.verificationTokenExpire = generateVerificationTokenExpire()
        // the encrypted password
    inRegistration.password = password
        //currently working
    const verificationToken = generateVerificationToken()
    const verificationTokenExpire = generateVerificationTokenExpire()
        //overwrite the inRegistration password with the hashed version
    inRegistration.password = password
        //this seems to rely on the order the data is passed in
        //TODO: [UA-5] make it an object we use to create the user
    var email = inRegistration.email
    return await user.create({ email, password, verificationToken, verificationTokenExpire })
        .then((data) => {
            return data
        }).catch((error) => {
            throw error
        })
}

/**
 * find the user based on the email address
 * used in the login form
 * could also be a user name 
 * @param {*} email 
 * @returns 
 */
async function GetUser(email) {
    return await user.findOne({ email })
        .then((data) => {
            //console.log(data)
            return data
        }).catch((error) => {
            throw error
        })
}

/**
 * checks the incoming request
 * only allows users with scope admin to access certain routes
 * called in index js to protect the api/route
 * 
 * Could be more flexible so you can pase a role and validate access for the role
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
const adminScope = (req, res, next) => {
    if (req.user.scope !== 'admin') {
        return res.status(403).send({
            status: 'fail',
            message: 'Invalid scope!'
        })
    }
    next();
}


/**
 * extract tokes from the request
 * @param {*} req 
 * @returns 
 */
const tokenExtractor = function(req) {
    let token = null
    if (req.req && req.req.cookies && req.req.cookies['auth._token.local']) {
        const rawToken = req.req.cookies['auth._token.local'].toString()
        token = rawToken.slice(rawToken.indexOf(' ') + 1, rawToken.length)
    } else if (req && req.cookies && req.cookies['auth._token.local']) {
        const rawToken = req.cookies['auth._token.local'].toString()
        token = rawToken.slice(rawToken.indexOf(' ') + 1, rawToken.length)
    }
    return token
}

/**
 * local strategy 
 */
passport.use(
    new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        async function(email, password, done) {
            await GetUser(email)
                .then((Thisuser) => {
                    return Thisuser
                }).then(async(Thisuser) => {
                    // first some basic validation
                    //user exists?
                    if (!Thisuser) {
                        // user not found, suspicioos or typo
                        return done(null, false, { message: 'Authentication failed, no such user or wrong password' })
                    }
                    //is the account locked?
                    if (Thisuser.isLocked) {
                        return done(null, false, { message: 'Account has been locked, please contact the administrator' })
                    }
                    //is the password being reset?
                    if (Thisuser.resetPassword) {
                        return done(null, false, { message: 'Reset of your password has been requested, please follow the link in the email you received.' })
                    }
                    ///validate the password
                    const validation = await comparePasswords(password, Thisuser.password)
                    if (validation && Thisuser.isVerified) {
                        Thisuser.failedLogins = 0
                        Thisuser.save()
                        return done(null, Thisuser)
                    } else if (validation) {
                        return done(null, false, {
                            message: '',
                            resendToken: true
                        })
                    } else {
                        //warn admin?
                        //log it anyway
                        Thisuser.failedLogins = Thisuser.failedLogins + 1
                        var result = await Thisuser.save()
                        console.log(Thisuser)
                        if (Thisuser.failedLogins > 2) {
                            return done(null, false, { message: 'If you have forgotten your password please reset your password, otherwise your account wil be locked' })
                        }
                        if (Thiuser.failedLogins > 4) {
                            Thisuser.resetPassword = true
                            const result = await Thisuser.save()
                            if (useSendgrid) {
                                //using sendgrid, untested

                                await MailSendgrid.SendAccountTempLock(
                                    Thisuser.email,
                                    'Account temporary locked'
                                )
                            } else {
                                //local email server
                                //TODO: [UA-24] send email to admins
                                //and add unluck function or extend it to send password reset email 
                                //
                                await MailLocal.SendAccountTempLock(
                                    Thisuser.email,
                                    'Account temporary locked',
                                )
                            }
                            return done(null, false, { message: 'Too many failed attempts, account locked, please contatc your admin' })
                        }
                        return done(null, false, { message: 'Authentication failed' })
                    }
                }).catch((err) => {
                    return done(err)
                })
        }
    )
)

/**
 * jwt strategy
 */
passport.use(new JwtStrategy({
        jwtFromRequest: tokenExtractor,
        secretOrKey: authUserSecret
    },
    function(jwtPayload, done) {
        return GetUser(jwtPayload.email)
            .then((user) => {
                if (user) {
                    return done(null, {
                        // add all the data you want to pass back
                        // for profile editing for profile data use /api/auth/user to get the record
                        // nonauthorized gives
                        /*{"message": {
                                "name": "JsonWebTokenError",
                                "message": "jwt malformed"
                            }
                        }
                        */
                        email: user.email,
                        scope: user.scope,
                        isLocked: user.isLocked
                    })
                } else {
                    return done(null, false, 'Failed')
                }
            })
            .catch((err) => {
                return done(err)
            })
    }));

function signUserToken(inUser) {
    return jwt.sign({
        id: inUser.id,
        email: inUser.email
    }, authUserSecret)
}


/**
 * token geneartion and verification
 * @returns 
 */
function generateVerificationToken() {
    return crypto.randomBytes(30).toString('hex')
}

function generateVerificationTokenExpire() {
    return new Date(Date.now() + expireSpan)
}

function signVerificationToken(email, verificationToken) {
    return jwt.sign({
        email,
        verificationToken
    }, authEmailVerificationSecret)
}

function verifySignedVerificationToken(token) {
    return jwt.verify(token, authEmailVerificationSecret)
}



/**
 * encrypts the password
 * @param {*} plainPassword 
 * @returns 
 */
async function generatePasswordHash(plainPassword) {
    return await bcrypt.hash(plainPassword, 12)
}

/**
 * compare the passwords
 * @param {*} plainPassword 
 * @param {*} hashedPassword 
 * @returns 
 */
async function comparePasswords(plainPassword, hashedPassword) {
    return await bcrypt.compare(plainPassword, hashedPassword)
}





export default {
    registerUser,
    CreateUser,
    GetUser,
    generatePasswordHash,
    signUserToken,
    adminScope,
    generateVerificationToken,
    generateVerificationTokenExpire,
    signVerificationToken,
    verifySignedVerificationToken,
    comparePasswords

}