// using free account of sendgrid 
//https://docs.sendgrid.com/for-developers/sending-email/quickstart-nodejs
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const from = process.env.SMTPEMAILFROM

const emailUrl = process.env.EMAILURL
const registerConfirmation = process.env.REGISTERCONFRIMATION
const resetConfirmation = process.env.RESETCONFRIMATION
const resetPassword = process.env.RESETPASSWORD

async function SendEmail(to, subject, text, html) {

    const msg = {
        to,
        from,
        subject,
        text,
        html
    }
    await sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent')
        })
        .catch((error) => {
            console.error(error)
        })
}

function stripHtmlTags(html) {
    return html.replace(/(<([^>]+)>)/gi, "");
}

async function SendRegistrationToken(to, subject, verificationToken) {
    const html = `<p>Please click into the link below to confirm your email address and finish the registration</p><a href="${emailUrl}${registerConfirmation}?token=${verificationToken}">Active your account</a>`
    const text = stripHtmlTags(html)
    await SendEmail(to, subject, text, html)
}

async function SendPasswordChangeToken(to, subject, verificationToken) {
    const html = `<p>Please click into the link below to change your password</p><a href="${emailUrl}${resetConfirmation}?token=${verificationToken}">Change your password</a>`
    const text = stripHtmlTags(html)
    await SendEmail(to, subject, text, html)
}

async function SendAccountTempLock(to, subject, verificationToken) {
    const html = `<p>Your account has temporary been locked. Please click  the link below to request a password change</p><a href="${emailUrl}${resetPassword}">Change your password</a>`
    const text = stripHtmlTags(html)
    await SendEmail(to, subject, text, html)
}

export default {
    SendEmail,
    SendRegistrationToken,
    SendPasswordChangeToken,
    SendAccountTempLock
}