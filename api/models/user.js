/**
 * basic user table in mongo db 
 * allows for user verification, password reset
 * 
 * only the basics, add more fields as needed
 */

const mongoose = require('mongoose')
const { Schema } = require('mongoose')

const userSchema = new Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    verificationToken: { type: String },
    verificationTokenExpire: { type: Date },
    isVerified: { type: Boolean, required: true, default: false },
    resetPassword: { type: Boolean, required: true, default: false },
    failedLogins: { type: Number, required: true, default: 0 },
    isLocked: { type: Boolean, required: true, default: false },

    scope: { type: String, default: 'user' },
    /**
     * other fields as required
     */
    username: { type: String, unique: true },

}, {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
})

/**
 * user middle ware
 * on update:  
 schema.pre('update', function(next) {
        const modifiedField = this.getUpdate().$set.field;
        if (!modifiedField) {
            return next();
        }
        try {
            const newFiedValue = // do whatever...
            this.getUpdate().$set.field = newFieldValue;
            next();
        } catch (error) {
            return next(error);
        }
    });
 *
 */

// on update
// does the isLocked change to true
// if so is the user same as the $this.authuser  
// then set islocked false
//send message, you cannot lock yourself out
// does the isLocket change to false



const user = mongoose.model("user", userSchema);

module.exports = { user: user }