/**
 * middleare to protect a page or layout against unauthorized access
 * use in all pages or layouts that should be accesible to logged in users only
 * as 
 * export default {
 *  middleware: ["authuser"],
 *  ... 
 */
export default function({ $auth, redirect }) {
    const user = $auth.user
    console.log(user)
    if (user) {} else {
        redirect('/')
    }

}