/**
 * middleare to protect a page or layout against unauthorized access
 * use in all pages or layouts that should be accesible to logged in and admin users only
 * as 
 * export default {
 *  middleware: ["authAdmin"],
 *  ... 
 */
export default function({ $auth, redirect }) {
    const user = $auth.user
    console.log(user)
        // logged in and with scope admin
    if (user && user.user.scope === 'admin') {} else {
        redirect('/')
    }
}