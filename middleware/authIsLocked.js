/**
 * TODO: [UA-8] Find elegant way to log out in the front end if the user if the account is locked in the backend
 * Problem here is that we can see in teh profile the user account is locked
 * This does not reflect in the JWT token
 * only when the enduser refreshes the page this gets reflected in the page
 * and the user is properly logged out
 * 
 * Through interfcae: lock account of yser
 * update store lockedout user
 * 
 * in middleware:
 * check if user is is lockedoutuser,
 * if so log user out 
 * @param {*} param0 
 */

export default function({ $auth, redirect }) {
    // Workaround, this does refresh the user.
    //  $auth.fetchUser()
    const user = $auth.user
    console.log(user)
    if (user && user.user) {
        console.log("Account locked: " + user.user.isLocked)
    }

    // this only works if the user refresshes the whole page

    if (user && user.user.isLocked) {
        console.log("user is locked out, checked in middleware")
        $auth.logout();
        //console.log($route)
        //this will loop unless we call another layout
        //redirect('/login/isLocked')
        //$auth.logout()

    } else if (user) {
        console.log("user is defined")

    }

}