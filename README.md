# UserAuth: Basic/generic user registration and athentication #

This repo gives you the basics of authentication (based on local and jwt) and registration of users.
It can be added to any project that needs user registration and authentication and extended as needed.
## Updates ##
**Dec 3 2021** Create sample users with [RANDOM USER GENERATOR](https://randomuser.me/) by making a call to /api/auth/createusers, see below for more details.

#### Small Stuff ####
- Finally got rid of the dreaded 'Cannot overwrite user model once compiled'.
- Added timestamps to the schema (this might cause issues if you already had users in your table, please recreate the user table, which is easy, see below).
- Changed accountlocked to isLocked.
- Updated the edit user form.
- some bug fixes that went unnoticed, apologies for the inconvenience.

### Sample Users ###

You can now create sample users easily, set the number of users you want to create and map the fields you want in your user table for further testing.
The users will be automaticly verified but emails will be send.
This function should be removed for production.
The url is /api/auth/createUsers, sample user details at [RANDOM USER GENERATOR](https://randomuser.me/)
The default password is test.



### Easy email sending and checking ###
You can create an account at [Etheral](https://ethereal.email/) for easier email testing for fake users during testing.


## Using it in your own projects ##
This is meant as drop in code for your own projects.
The pages as as basic as they get and use vuetify/vue.
I have not checked if they work properly under vue itself. I user Nuxt/Vuetify/express.

I you implement the code in your own project ensure that all the modules are installed.


## Next Steps ##
Currently this project is under heavy development. I am considering adding Google authentication and maybe MFA and oath authentication.
The project could do with some rewriting in regards to naming and organization of files.


---
## Prerequisites ##

An account at sendgrid or a mailserver you can use. The app sends out confirmation emails and password reset emails.
You can also use [ethereal.email](https://ethereal.email/) for testing/sending emails. 
MongoDB setup.

## configuration ##
All configuration can be done in the .env, a sample.env is supplied.
Nuxt Config uses env settings too.

## User table is as basic as it can get ##

The user table setup only supplies the minimal amount of fields: email, password, scope and some keys.
This can be extended to your needs, ensure that the forms are updated accordingly.

## After installation ##

Ensure your .env has the correct setup, connection to database, email configuration, and/or the required api keys.
Start with: npm run dev.
Create an account, verify account and login.

## Usage ##

A general overview.

### Scope ###
The default scope is 'user', create another user and set the scope to admin (directly on the table or through mongoose compass).
As user you are not able to access the inspire page, due to middleware: `["authuser"]`
As user you can access the user page but the table won't load (yes, I have to add error checking when it retrieves data).
The data is loaded through api/admin, which also has scope set to admin only.
Login as a user with admin scope and you will be able to see the Inspire page and the User page and the user table.

## use case/scenario
An app that needs user registration/management and user login/logout and a scope/role.
You need to limit access to pages:
- public pages
- logged in users only: 
- private data of the user where they can change their info (a profile page)
- admin users

All these scenarios are provided for.

### page protection ###

There are two middleware scripts that limit access to pages:
Check the `/midleware` directory:
Limit access to pages for logged in users only: `/pages/profile.vue` 
```
<script>
export default {
  // only logged in users should be able to get this page
  middleware: ["authuser"],
};
</script>
```

Limit access to pages for admin users only: `/pages/users.vue` 
```
<script>
export default {
  middleware: ["authadmin"],
  data: () => ({}),
  ...
};
</script>
```

### Api protection ###

The same principles apply to the api too.
- public api
- api acccesible for logged in users only: See `api/index,.js` for details on how the route to `user` is set.
- api for admin users only: see `api/index.js`  for details on how the route to `admin` is set to protect the path with jwt authentication and a required scope

### Show/hide page elements/menu's based on scope/loggedin ###

Certain element's visibility is based on if the user is logged in or is an admin.

***Not Logged in:*** _Login_, _Register_ and _Reset Password_ are dsiplayed in the top bar.

***Logged in*** _Profile_ and _Log Out_ are displayed in the top bar.

### user management ###

There is a basic table to display and edit users, mainly the ability to lock or unlock users. 
An account will get locked after 5 failed tries, the user must then contact the admin to get their account unlocked (5 failed logins do smell suspicous so there could be some password sprying going on).
The user can request a password reset if needed. An email will be send to the supplied email address and the user can reset the password from there.

## Please Check ##

In the supplied code the password rules are commented out, this allows for easier user creation during testing. See `/components/forms/newpassword.vue` and `/components/forms/changepassword.vue` and uncomment/change the password rules as needed.

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).